/****************************************************************************
 * Project                               : shakti devt board
 * Name of the file                      : uart.c
 * Brief Description of file             : src  file for uart
 * Name of Author                        : Kotteeswaran and Niketan Shahapur
 * Email ID                              : <kottee.1@gmail.com>  <niketanshahpur@gmail.com>

 Copyright (C) 2019  IIT Madras. All rights reserved.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

*****************************************************************************/

#include "uart.h"
#define UART0_BASE_ADDRESS 0x11300
uart_struct *uart_instance[0];

/**
 * @fn getchar()
 * @brief Function to read a single character from the standard input device.
 * @details This function will be called to write a single character from the stdin.
 * @param[in] No input parameters.
 * @param[Out] Character as int.
 */
#undef getchar
int getchar()
{
	while((uart_instance[0]->status & STS_RX_NOT_EMPTY) == 0); 
	return (uart_instance[0]->rcv_reg);
}

/**
 * @fn int putchar
 * @brief Function to write a single character to the standard output device.
 * @details This function will be called to print a single character to the stdout by passing 
 * character as an integer.
 * @param[in] character as int.
 * @param[Out] int
 */
#undef putchar
int putchar(int ch)
{
	uart_instance[0] = (uart_struct*) 0x11300 ;
	while(uart_instance[0]->status & STS_TX_FULL);
	uart_instance[0]->tx_reg = ch;
	return 0;
}
