#include<stdint.h>

char string[] = "This Is $hakti, The 1st Ever 'Made In India' Computer Chip";
char result[100]={1,2,3,4,5,5};
volatile int flag = 1;

int main()
{
    while (flag);

    int i = 0;

    while (i < 100) {
        if (string[i] >= 'a' && string[i] <= 'z')
            result[i] = 'l';
        else if (string[i] >= 'A' && string[i] <= 'Z')
            result[i] = 'u';
        else if (string[i] >= '0' && string[i] <= '9')
            result[i] = 'n';
        else 
            result[i] = 's';

        i++;
    }

end:
    while (!flag);
}
