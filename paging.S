/***************************************************************************
 * Project           		: Learn with Shakti
 * Name of the file	     	: paging.S
 * Created date			:  
 * Brief Description of file    : Demonstrate paging, by mapping VA to PA identically
 * Name of Author    	        : Sathya narayanan
 * Email ID                     : sathya281@gmail.com 

 Copyright (C) 2020  IIT Madras. All rights reserved.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

 ***************************************************************************/
# brief problem description
#1. Demonstrate that enabling page translation has no effect in M mode.
#2. Demonstrate that setting up satp with righ values only make write success.
#3. Demonstrate that in S mode, after writing satp succsessfully and do a "si".
#4. Demonstrate paging in S mode, identical mapping

# brief soln description
# overall soln is satp is the register that has the Translation mode and rootpage table physical address. From there build the entries for all 3 levels of paging. After building the entries properly, we will see si after pc at csrw satp,t0, works. Before that it would throw a instruction page fault.
# primarily, we will be using mstatus to switch modes and satp to setup root pt phy addr . Translation strictly follows the riscv priv spec

#if __riscv_xlen == 64
#define LREG ld
#define SREG sd
#define REGBYTES 8
#else
#define LREG lw
#define SREG sw
#define REGBYTES 4
#endif

#define STACK_BASE_ADDR 0x10010900

#machine mode 
_start:	
li sp, STACK_BASE_ADDR  #set sp
la t0, trap_entry        
csrw mtvec, t0        #set mtvec with trap_entry       

li t0, 0x10020000
li t1, 0x4008401
sd t1,0(t0)

li t0, 0x10021400
li t1, 0x4008801
sd t1,0(t0)

li t0, 0x10022080
li t1, 0x400404f
sd t1,0(t0)

#rootpte
#	srli t1,t1,12

#	or t0, t1, t0
#	csrw satp, t0

#procedure to edit the prev priv mode
li      t0, 0x1800
csrrc   zero,mstatus, t0       # clear MPP[1:0] bits
li      t0, 0x0800
csrs    mstatus, t0          # set mpp (previous mode) with supervisor mode

#setting the Entry point in S mode
la  t0, s_mode_begin         # set up mepc with addr of S mode re-entry func. 
csrw    mepc, t0               # writing s_mode_begin into mepc
mret                         #exiting M mode 

#trap handler
trap_handler:
csrr t0,mcause
li t3, 0x80000000
and t0,t0,t3
beqz t0, exception_handler
1:      ret


exception_handler:
csrr t0,mcause
la t1, _data1  #base address
lw t2, 0(t1)  # offset value
	addi t2,t2,4 #t1 holds base address for storing trap occurences
sw t2,0(t1)  
	addw t1, t1,t2
sw t0,0(t1)  
	j 1b

	.p2align 2
	trap_entry:     # currently trap_entry has a nop. Later we will add actual instrns
	addi sp, sp, -32*8
	nop
#x0 is always 0
	sd x1, 1*8(sp)
	sd x2, 2*8(sp)
	sd x3, 3*8(sp)
	sd x4, 4*8(sp)
	sd x5, 5*8(sp)
	sd x6, 6*8(sp)
	sd x7, 7*8(sp)
	sd x8, 8*8(sp)
	sd x9, 9*8(sp)
	sd x10, 10*8(sp)
	sd x11, 11*8(sp)
	sd x12, 12*8(sp)
	sd x13, 13*8(sp)
	sd x14, 14*8(sp)
	sd x15, 15*8(sp)
	sd x16, 16*8(sp)
	sd x17, 17*8(sp)
	sd x18, 18*8(sp)
	sd x19, 19*8(sp)
	sd x20, 20*8(sp)
	sd x21, 21*8(sp)
	sd x22, 22*8(sp)
	sd x23, 23*8(sp)
	sd x24, 24*8(sp)
	sd x25, 25*8(sp)
	sd x26, 26*8(sp)
	sd x27, 27*8(sp)
	sd x28, 28*8(sp)
	sd x29, 29*8(sp)
	sd x30, 30*8(sp)
sd x31, 31*8(sp)

	jal trap_handler

	ld x1, 1*8(sp)
	ld x2, 2*8(sp)
	ld x3, 3*8(sp)
	ld x4, 4*8(sp)
	ld x5, 5*8(sp)
	ld x6, 6*8(sp)
	ld x7, 7*8(sp)
	ld x8, 8*8(sp)
	ld x9, 9*8(sp)
	ld x10, 10*8(sp)
	ld x11, 11*8(sp)
	ld x12, 12*8(sp)
	ld x13, 13*8(sp)
	ld x14, 14*8(sp)
	ld x15, 15*8(sp)
	ld x16, 16*8(sp)
	ld x17, 17*8(sp)
	ld x18, 18*8(sp)
	ld x19, 19*8(sp)
	ld x20, 20*8(sp)
	ld x21, 21*8(sp)
	ld x22, 22*8(sp)
	ld x23, 23*8(sp)
	ld x24, 24*8(sp)
	ld x25, 25*8(sp)
	ld x26, 26*8(sp)
	ld x27, 27*8(sp)
	ld x28, 28*8(sp)
	ld x29, 29*8(sp)
	ld x30, 30*8(sp)
ld x31, 31*8(sp)

	mret



#supervisor code
	s_mode_begin:

#sample instrns
	la x17, _data1  # base address
	sw x17, 0x4(x17) # Store word - Storing Word 

	andi t0,t0,0
	addi t0,t0,8
	slli t0,t0,60

	li t1,0x10020000
#rootpte
	srli t1,t1,12

	or t0, t1, t0
	csrw satp, t0

#trying to switch to U mode
x:	li      t0, 0x0100
	csrc    sstatus, t0          # set spp (previous mode) with usor mode
	la  t0, u_mode_begin         # set up sepc with addr of U mode re-entry func.

	csrw    sepc, t0              #writing u_mode_begin into mepc
	sret                         #exiting S mode 

	u_mode_begin:
	ret

.p2align 12  #aligning to page boundary
rootpte:
.dword 0










_data1:
.word 	0
.word 	0
.word 	0
.word 	0
