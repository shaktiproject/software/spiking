all:
	@riscv64-unknown-elf-gcc -g -Og -S sample.c 
	@riscv64-unknown-elf-gcc -S sum.c 
	@riscv64-unknown-elf-gcc -S add3.c 
	@riscv64-unknown-elf-gcc add3.c -o add3.elf 
	@riscv64-unknown-elf-gcc -S add1.c 
	@riscv64-unknown-elf-gcc add1.c -o add1.elf 
	@riscv64-unknown-elf-gcc -S add2.c 
	@riscv64-unknown-elf-gcc add2.c -o add2.elf
	@riscv64-unknown-elf-gcc -g -Og -o deploy.o -c sample.c 
	@riscv64-unknown-elf-gcc -g -o sum.o -c sum.c 
	@riscv64-unknown-elf-gcc -g -Og -T spike.lds -nostartfiles -o sample.elf deploy.o
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds paging.S -o paging.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds pmp.S -o pmp.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds pmp1.S -o pmp1.elf
	@riscv64-unknown-elf-gcc -g -T spike.lds -nostartfiles -o sum.elf sum.o
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds example1.S -o example1.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds example2.S -o example2.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds example3.S -o example3.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds fact.S -o fact.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds bootload.S -o bootload.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds switch_mode.S -o switch_mode.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds trap_mode.S -o trap_mode.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds mtime_trap.S -o mtime_trap.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds mtime_trap1.S -o mtime_trap1.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds trap_example.S -o trap_example.elf
	@riscv64-unknown-elf-gcc -nostdlib -nostartfiles -T spike.lds medeleg_trap.S -o medeleg_trap.elf
	@riscv64-unknown-elf-gcc -g hello.c -o hello.elf 

clean:
	rm *.elf *.o *.s
