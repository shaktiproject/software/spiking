/***************************************************************************
* Project           			:  Learn with Shakti
* Name of the file	     		:  bootload.S
* Created date			        :  
* Brief Description of file             :  A simple effective bootcode. This loops over
					   same instruction in infinite loop. Now, any
                                           program can be loaded through gdb and
                                           debugged. This program is written to help 
                                           spike simulation easier
* Name of Author    	                :  Dr. V. Kamakoti 
* Email ID                              :  veezhi@gmail.com

    Copyright (C) 2020  IIT Madras. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

***************************************************************************/

_start: 
    addi x10, x0, 1
    bnez x10, _start # jump to _start
