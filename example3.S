/****************************************************************************
* Project           			:  Learn with Shakti
* Name of the file	     		:  example3.S
* Created date			        :  
* Brief Description of file             :  Loading value from memory sequentially.
* Name of Author    	                :  
* Email ID                              :  

    Copyright (C) 2020  IIT Madras. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/
_start:	
# Shift right arithmetic immediate - Shifting X0 right by 1 bit and store it to x17
# Initializing the registers to 0 
	srai x17, x0,1	
	srai x12, x0,1	
	srai x10, x0,1
	srai x15, x0,1
	srai x6, x0,1

# Add immediate - Adding constant to source register and saving it in destination register 
	addi  x10, x10, 1      
	addi  x12, x10, 13     
	addi  x17, x10, 64

# Loading the constants from _data section     
	la x15, _data1 #store _data1 loc to x15
	addi x17,x0, 0x10 #comparing register for end of loop
	addi x14,x0, 0x0   #index
loop:	lw x16, 0(x15) #load value from x15 pointing location to x16 reg
	addi x15, x15, 0x04 #goto next location
	addi x14, x14, 0x04
        bne x14,x17,loop #check for equality
        
#Have x15 with data address
#then increment data address
#load value
#loop for 4 iterations

# Store word - Storing Word 
	sw x17, 0x60(x15) 
#load x15+0x60 location value to x12
	lw x12, 0x60(x15)
	bnez x10, _start #if x10 not equal to zero, jump to _start

.p2align 0x2   #this make sures the _data1 starts from a 4 byte aligned boundary
.section .data 
_data1:               #starting location of data
.word 	7
.word 	6
.word 	5
.word 	4
